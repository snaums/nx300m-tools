from misc import *
import requests
import os

class Connection ( object ):
    def __init__ ( self, ip, apiPort, controlPort ):
        self.ip = ip
        self.apiPort = apiPort
        self.controlPort = controlPort
        self.name = "FON"

    def req ( self, fn, filename ):
        if filename.startswith ("/"):
            filename = filename[1:]
        path = "http://%s:%d/%s"%(self.ip, self.apiPort, filename)
        print ("GET: ", path)
        r = requests.get (
                path, 
                headers=self.prepareHeader( self.apiPort ),
                timeout = 1)
        if r.status_code != 200 :
            print ("Cannot get ", filename, ": ", r);
            raise ConnectionError
        return r

    def prepareHeader ( self, port ):
        return {
            "User-Agent": "SEC_RVF_ML_%s"%(self.name),
            "Host": "%s"%(self.ip)
        }

    def cachedGet ( self, filename, save=True ):
        if filename.startswith("/") :
            filename = filename[1:]
        if os.path.isfile ( filename ):
            with open(filename, "r") as f:
                txt = f.read()

            print ( "got %s from cache"%filename )
            return txt
        else:
            r = self.get ( filename, save )
            return r.text

    def get ( self, filename, save=False ):
        r = self.req ( requests.get, filename )
        if save == True:
            with open(filename, "w") as f:
                f.write ( r.text )
        return r

    def post ( self, path, header, request ):
        if path.startswith("/"):
            path = path[1:]
        path = "http://%s:%d/%s"%(self.ip, self.apiPort, path)
        hdr = {
            "Host": "%s"%(self.ip)
        }
        for k,v in header.items ():
            hdr[k] = v

        txt = request.encode ( "utf-8" )
        ##print ( "POST: ", path, hdr, request )
        return requests.post ( path, data=txt, headers=hdr )
        

    def head ( self, filename ):
        return self.req ( requests.head, filename )


