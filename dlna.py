## obviously I'm writing my own soap client here... *looks annoyed*

import xml.etree.ElementTree as ET
import sys
from misc import *

class dlnaService (dlnaInternalObject):
    __name__ = ["serviceType", "serviceId", "controlURL", "eventSubURL", "SCPDURL"]
    def __init__ ( self ):
        for n in self.__name__:
            setattr(self, n, None )

    def sanity ( self ):
        if self.serviceType == "" or self.serviceId == "" or self.controlURL == "" or self.eventSubURL == "" or self.SCPDURL == "" :
            return False

        return True


class dlnaDevice (dlnaInternalObject):
    __name__ = ["deviceType", "friendlyName", "manufacturer", "manufacturerURL", "modelDescription", "modelName", "modelNumber", "modelURL", "serialNumber", "UDN"]
    def __init__ ( self, serviceList ):
        for n in self.__name__:
            setattr(self, n, None )
        self.serviceList = serviceList

def parseDLNAString ( string ):
    '''parse SCPD from string, don't use in production'''
    root = ET.fromstring ( string )
    return parseDLNA ( root )

def parseDLNA ( root ):
    serviceList = []
    for service in root.iter ( '{urn:schemas-upnp-org:device-1-0}service' ):
        s = dlnaService()
        for child in service:
            s.addAttribute ( child.tag, child.text )

        if s.sanity():
            serviceList += [s]

    device = dlnaDevice ( serviceList )
    for dev in root.iter( "{urn:schemas-upnp-org:device-1-0}device" ):
        for child in dev:
            device.addAttribute ( child.tag, child.text )

    return device

def parseDLNAFile ( filename ):
    device = None
    with open(filename, "r") as f:
        txt = f.read ()
        device = parseDLNAString ( txt )

    return device

if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        print ( parseDLNAFile ( filename ))
