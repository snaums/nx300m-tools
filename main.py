#!/usr/bin/python3

import random
import sys

from soap import *
from connection import *

ip = "192.168.107.1"
controlPort = 7788
apiPort = 7676

print ("%s:%d/mode/control"%(ip, controlPort))
r = requests.head ( "http://%s:%d/mode/control"%(ip, controlPort) )
if r.status_code != 200 :
    print ( "An error occurred ", r );
    sys.exit(1);

conn = Connection ( ip, apiPort, controlPort )
soap = initSoap ( "smp_2_", conn );

if "-v" in sys.argv or "--list-functions" in sys.argv:
    print ( soap )
    sys.exit(1)

#req = soap.ConnectionManager.GetProtocolInfo()
#print ( req.text )
#print ( req )

print ( soap )


req = soap.ConnectionManager.GetProtocolInfo()
print ( req.text )
print ( req )
req = soap.ConnectionManager.GetCurrentConnectionIDs()
print ( req.text )
print ( req )
import pdb
pdb.set_trace()
req = soap.ConnectionManager.GetCurrentConnectionInfo(ConnectionID="0")
print ( req.text )
print ( req )
