def genericStr ( obj ):
    ## prints all attributes of the config-object
    result = ""
    for a in dir( obj ):
        if ( len(a) <= 0 or a[0] == "_" ) :
            continue

        if ( type(getattr(obj,a)) is int ):
            result = result + "%s = %d\n"%(a, getattr(obj,a))
        elif ( type(getattr(obj,a)) is str ):
            result = result + "%s = %s\n"%(a, getattr(obj,a))
        elif ( callable(getattr(obj,a)) ):
            pass
        elif ( type(getattr(obj,a)) is list ):
            result = result + "%s = [ \n"%(a)
            for l in getattr(obj,a):
                result = result + "    " + str(l) + ",\n";
            result = result[:-2] + "]\n";
        else:
            result = result + "%s = %s\n"%(a, str(getattr(obj, a)))
    return (result)

def addAttribute ( obj, attr, val ):
    _addAttribute ( obj, attr, val, obj.__name__, obj.__prefix__ )

def _addAttribute ( obj, attr, val, names, prefixes ):
    for p in prefixes:
        if attr.startswith(p):
            attr = attr[len(p):]
            break

    if attr in names:
        setattr ( obj, attr, val )
    else:
        pass

class dlnaInternalObject :
    __prefix__ = ["{urn:schemas-upnp-org:device-1-0}","{urn:schemas-dlna-org:device-1-0}","{http://www.sec.co.kr/dlna}","{urn:schemas-upnp-org:service-1-0}"]
    def __str__ ( self ):
        return genericStr ( self )

    def addAttribute ( self, attr, val ):
        return addAttribute ( self, attr, val )

    def prfx ( self, val ):
        for p in self.__prefix__:
            if val.startswith(p):
                return val [len(p):]
        return val

    def isTag ( self, tag, against ):
        return self.prfx(tag) == against;

