#!/usr/bin/python3

import requests
import random
import sys

ip = "192.168.107.1"
controlPort = 7788
apiPort = 7676

class Connection ( object ):
    def __init__ ( self, ip, apiPort, controlPort ):
        self.ip = ip
        self.apiPort = apiPort
        self.controlPort = controlPort
        self.name = "FON"

    def req ( self, fn, filename ):
        path = "http://%s:%d/%s"%(self.ip, self.apiPort, filename)
        print ( path )
        r = requests.get (
                path, 
                headers={ 
                    "User-Agent": "SEC_RVF_ML_%s"%(self.name), 
                    "Host": "%s:%d"%(self.ip,self.apiPort) 
                },
                timeout = 1)
        if r.status_code != 200 :
            print ("Cannot get ", filename, ": ", r);
            raise ConnectionError
        return r

    def get ( self, filename, save=False ):
        r = self.req ( requests.get, filename )
        if save == True:
            with open(filename, "w") as f:
                f.write ( r.text )
        return r

    def head ( self, filename ):
        return self.req ( requests.head, filename )

if len(sys.argv) >= 2:
    ip = sys.argv[1]
if len(sys.argv) >= 3:
    controlPort = int(sys.argv[2])

print ("%s:%d/mode/control"%(ip, controlPort))
r = requests.head ( "http://%s:%d/mode/control"%(ip, controlPort) )
if r.status_code != 200 :
    print ( "An error occurred ", r );
    sys.exit(1);

conn = Connection ( ip, apiPort, controlPort )

for i in range(1,10):
    try:
        r = conn.get ( "smp_%d_"%i, True )
    except Exception as e:
        print (e)

print ("Worked!")
