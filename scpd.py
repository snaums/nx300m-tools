import xml.etree.ElementTree as ET
import sys
from misc import *
from dlna import *

class scpdStateVar (dlnaInternalObject):
    __name__ = [ "name", "dataType", "defaultValue" ]
    def __init__ ( self, event=False ):
        self.name = ""
        self.dataType = ""
        self.defaultValue = None
        if type(event) is bool:
            self.sendEvents = event
        if event == "yes":
            self.sendEvents = True
        else:
            self.sendEvents = False

        self.allowedValues = None

    def addAllowedValues ( self, allowed ):
        if type(self.allowedValues) is not list:
            self.allowedValues = []
        self.allowedValues += [ allowed ]

    @staticmethod
    def parse ( node ):
        result = scpdStateVar( False )
        if not result.isTag ( node.tag, "stateVariable" ):
            raise TypeError
        
        try:
            if node.attrib["sendEvents"] == "yes":
                result.sendEvent = True
        except KeyError:
            pass

        for n in node:
            if result.isTag(node.tag, "AllowedValueList"):
                for ni in n:
                    result.addAllowedValues ( ni.text )
            else:
                result.addAttribute ( n.tag, n.text )

        return result;


class scpdArgument ( dlnaInternalObject ):
    __name__ = [ "name", "direction", "relatedStateVariable" ]
    def __init__ ( self ):
        self.name = ""
        self.direction = ""
        self.relatedStateVariable = ""

    @staticmethod
    def parse ( arg ):
        result = scpdArgument()
        if not result.isTag ( arg.tag, "argument" ):
            raise TypeError

        for n in arg:
            result.addAttribute ( n.tag, n.text )

        return result

class scpdAction ( dlnaInternalObject ):
    __name__ = [ "name" ]
    def __init__ ( self, name="" ):
        self.name = name
        self.argumentList = []

    def addArgument ( self, argument ):
        if type(self.argumentList) is not list:
            self.argumentList = []
        self.argumentList += [argument]

    def findArgument ( self, arg ):
        for a in self.argumentList:
            if arg == a.name:
                return a
        raise KeyError

    def numArguments ( self ):
        num = 0
        for a in self.argumentList:
            if a.direction == "in":
                num =+ 1

        return num

    @staticmethod
    def parse ( node ):
        result = scpdAction ()
        if not result.isTag(node.tag, "action"):
            raise TypeError

        for n in node:
            if result.isTag(n.tag, "argumentList"):
                for arg in n:
                    result.addArgument ( scpdArgument.parse ( arg ) )
            else:
                result.addAttribute ( n.tag, n.text )

        return result


class scpd ( dlnaInternalObject ) :
    def __init__ ( self, actionlist, StateVariableList ):
        self.serviceId = ""
        self.dlna = None
        self.stateVariableList = StateVariableList
        self.actionList = actionlist

    def findAction ( self, action ):
        for a in self.actionList:
            if a.name == action:
                return a
        raise KeyError

    def findVariable ( self, var ):
        for v in self.stateVariableList:
            if v.name == var:
                return v
        raise KeyError


def parseScpdString ( string ):
    root = ET.fromstring ( string )
    return parseScpd ( root )

def parseScpd ( root ):
    statevars = []
    for var in root.iter("{urn:schemas-upnp-org:service-1-0}stateVariable"):
        statevars += [scpdStateVar.parse ( var )]

    actions = []
    for action in root.iter("{urn:schemas-upnp-org:service-1-0}action"):
        actions += [scpdAction.parse ( action )]

    return scpd ( actions, statevars )

def parseScpdFile ( filename ):
    scpd = None
    with open ( filename, "r" ) as f:
        txt = f.read()
        scpd = parseScpdString ( txt )
    return scpd

if __name__ == "__main__":
    if len(sys.argv) > 1 :
        filename = sys.argv[1]
        parseScpdFile ( filename )
