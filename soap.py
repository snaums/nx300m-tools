import xml.etree.ElementTree as ET
import requests
import sys
from misc import *
from dlna import *
from scpd import *

def soapRequestMessage ( service, function, **arguments ):
    request = "<s:Body>\n  <u:%s xmlns:u=\"%s\">\n"%(function, service.dlnaService.serviceType)
    for k, v in arguments.items():
        request += "    <%s>%s</%s>\n"%(k, v, k)
    request += "  </u:%s>\n</s:Body>"%(function)
    return request

def soapRequest ( service, function, **arguments ):
    request = '<?xml version="1.0" encoding="utf-8"?>\n<s:Envelope xmlns:s="http://schemas.xmlsoap.org/soap/envelope/" s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">\n'
    request += soapRequestMessage ( service, function, **arguments )
    request += '\n</s:Envelope>'
    return request

def doSoapCall ( connection, service, function, **arguments ):
    toUrl = service.dlnaService.controlURL
    soapAction = "%s#%s"%(service.dlnaService.serviceType, function)
    request = soapRequest ( service, function, **arguments )
    #print ( request )
    
    return connection.post ( 
        toUrl, 
        { 
            "Content-Type": "text/xml; charset=\"utf-8\"",
            "Content-Length": str(len(request)),
            "SOAPACTION": "\"" + str(soapAction) + "\"",
            "Connection": "close"
        },
        request
    )


class SOAPNoServiceError ( Exception ):
    pass
class SOAPNotAnActionError ( Exception ):
    pass
class SOAPInvalidArgumentError ( Exception ):
    pass
class SOAPMissingArgumentError ( Exception ):
    pass

class SOAPArgument ( dlnaInternalObject ):
    def __init__ ( self, scpdarg, scpdstatevar ):
        self.name = scpdarg.name
        self.argument = scpdarg
        self.statevar = scpdstatevar

    def __str__ ( self ):
        return ( " \033[90m%s[%s]\033[0m \033[32m%s\033[0m "%(self.argument.name, self.argument.direction, self.statevar.dataType))


class SOAPAction ( dlnaInternalObject ):
    def __init__ ( self, service, scpd, connection, action ):
        self.service = service
        self.scpd = scpd
        self.connection = connection
        self.name = action.name
        self.action = action
        self.inArgs = {}
        self.outArgs = {}
        for arg in action.argumentList:
            var = self.scpd.findVariable ( arg.relatedStateVariable )
            a = SOAPArgument ( arg, var )
            if arg.direction == "in":
                self.inArgs[a.name] = a
            else:
                self.outArgs[a.name] = a

    def __call__ ( self, **kwargs ):
        ''' raises a KeyError if the arguments don't fit '''
        arguments = {}
        for k, v in kwargs.items():
            arg = None
            try:
                arg = self.inArgs[k]
            except KeyError:
                raise SOAPInvalidArgumentError
            # TODO check type

            arguments[k] = (v, arg.statevar.dataType)

        if len(arguments) != len(self.inArgs):
            print(SOAPMissingArgumentError()) 

        return doSoapCall ( self.connection, self.service, self.name, **arguments )

    def __str__ ( self ):
        result = "    "
        result += " \033[37;1m%s\033[0m ( "%(self.name)
        for k, inargs in self.inArgs.items():
            result += str(inargs) + ","

        result +=" )\n        -->"
        for l, out in self.outArgs.items():
            result += str(out)

        result += "\n"

        return result


class SOAPService ( dlnaInternalObject ) :
    __prefix__ = ["urn:upnp-org:serviceId:"]
    def __init__ ( self, scpd, dlna, connection ):
        self.serviceId = scpd.serviceId
        self.name = self.prfx(scpd.serviceId)
        self.connection = connection
        self.scpd = scpd
        self.actions = []
        self.dlnaService = None
        for service in dlna.serviceList:
            if service.serviceId == self.serviceId :
                self.dlnaService = service
                break

        if self.dlnaService == None :
            raise TypeError

        for action in scpd.actionList:
            a = SOAPAction ( self, scpd, connection, action )
            self.actions += [ a ]
            setattr ( self, action.name,a )

    def __call__ ( self, name, **args ):
        if hasattr ( self, name ):
            return getattr(self, name)( **args )
        else:
            raise SOAPNotAnActionError()

    def __str__ ( self ):
        result = "  Service %s {\n"%self.name
        for a in self.actions:
            result += str(a)
        return result


class SOAP ( dlnaInternalObject ):
    def __init__ ( self, dlna, scpds, connection ):
        self.scpd = scpds
        self.connection = connection
        self.services = []
        self.dlna = dlna
        for s in self.scpd:
            x = SOAPService ( s, dlna, connection )
            self.services += [ x ]
            setattr ( self, x.name, x )

    def findSCPD ( self, serviceId ):
        for s in self.__scpd:
            if s.serviceId == serviceId:
                return s
        raise KeyError

    def __call__ ( self, service, function, **kwargs ):
        for s in self.services:
            if s.name == service or s.serviceId == service:
                return s ( function, **kwargs )
        raise SOAPNoServiceError()

    def __str__ ( self ):
        result = "SOAP {\n"
        for s in self.services:
            result += str(s)
        result += "}\n"
        return result

def initSoap ( dlnafile , conn ):
    r = conn.cachedGet ( dlnafile )
    dlna_dev = parseDLNAString ( r )
    scpd = []
    for service in dlna_dev.serviceList:
        r = conn.cachedGet ( service.SCPDURL )
        t = parseScpdString ( r )
        t.dlna = dlna_dev
        t.serviceId = service.serviceId
        scpd += [ t ]

    return SOAP( dlna_dev, scpd, conn )
